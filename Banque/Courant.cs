﻿using System;

namespace Banque
{
	class Courant : Compte
	{
		#region Properties

		private double _ligneDeCredit;

		public double LigneDeCredit
		{
			get { return _ligneDeCredit; }
			set { _ligneDeCredit = Math.Abs(value); }
		}

		#endregion

		#region Methods

		public Courant(string numero, Personne titulaire, double ligneDeCredit = 0) : base(numero, titulaire)
		{
			LigneDeCredit = ligneDeCredit;
		}

		public override bool Retrait(double montant)
		{
			return base.Retrait(montant, LigneDeCredit);
		}

		protected override double CalculInteret()
		{
			return (Solde > 0) ? Solde*0.3 : Solde*0.975;
		}

		#endregion
	}
}