﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque
{
	class Program
	{
		static void Main(string[] args)
		{
			Banque b = new Banque("Test", 10);
			Courant c = new Courant("1234", new Personne("Boudron", "Boris", new DateTime(1988, 10, 7)));
			b.Ajouter(c);
			bool continuer = true;
			do
			{
				Console.WriteLine("Operations :");
				Console.WriteLine("Créer un compte (1) :");
				if (b.Comptes.Count > 0)
				{
					Console.WriteLine("Accéder à un compte (2) :");
				}

				Console.WriteLine("Quitter (q) :");

				bool goodChoice = false;
				while (!goodChoice)
				{
					ConsoleKeyInfo pressed = Console.ReadKey();
					switch (pressed.Key)
					{
						case ConsoleKey.NumPad1:
						{
							goodChoice = true;
							Console.WriteLine();
							Console.WriteLine("Nom :");
							string nom = Console.ReadLine();
							Console.WriteLine("Prénom :");
							string prenom = Console.ReadLine();
							Console.WriteLine("Date de naissance :");
							DateTime dateNaiss;
							while (!DateTime.TryParse(Console.ReadLine(), out dateNaiss))
							{
								Console.WriteLine("Mauvais format. Réessayez.");
							}

							Console.WriteLine("Compte épargne (1) ou courant (2) ?");
							bool goodAccountChoice = false;
							while (!goodAccountChoice)
							{
								ConsoleKeyInfo compte = Console.ReadKey();
								Console.WriteLine();
								switch (compte.Key)
								{
									case ConsoleKey.NumPad1:
									{
										goodAccountChoice = true;
										Epargne newE = new Epargne(b.GenererNumero(),
											new Personne(nom, prenom, dateNaiss));
										b.Ajouter(newE);
										break;
									}
									case ConsoleKey.NumPad2:
									{
										goodAccountChoice = true;
										Courant newC = new Courant(b.GenererNumero(),
											new Personne(nom, prenom, dateNaiss));
										b.Ajouter(newC);
										break;
									}
									default:
									{
										Console.WriteLine("Choix invalide. Recommencez.");
										break;
									}
								}
							}

							break;
						}
						case ConsoleKey.NumPad2:
						{
							goodChoice = true;
							Console.WriteLine();
							if (b.Comptes.Count > 0)
							{
								bool continuerActions = true;
								while (continuerActions)
								{
									continuerActions = ActionsDuCompte(b);
								}
							}
							else
							{
								Console.WriteLine("Choix invalide.");
							}

							break;
						}
						case ConsoleKey.Q:
						{
							goodChoice = true;
							continuer = false;
							break;
						}
						default:
						{
							Console.WriteLine("Choix invalide.");
							break;
						}
					}
				}
			} while (continuer);

			Console.WriteLine();
			Console.WriteLine("Au revoir !");
			Console.ReadKey();
		}

		public static bool ActionsDuCompte(Banque b)
		{
			bool exit = false;
			Console.WriteLine("Entrez votre numéro de compte : ");
			Compte toAccess = DemanderNumero(b);

			Console.WriteLine("Voulez-vous effectuer un retrait (1), un dépôt (2) ou retourner au manu principal (r) ?");
			bool goodOperationChoice = false;
			while (!goodOperationChoice)
			{
				ConsoleKeyInfo operation = Console.ReadKey();
				Console.WriteLine();
				switch (operation.Key)
				{
					case ConsoleKey.NumPad1:
					{
						goodOperationChoice = true;
						int montant = DemanderMontant("retirer");
						toAccess.Retrait(montant);
						break;
					}
					case ConsoleKey.NumPad2:
					{
						goodOperationChoice = true;
						int montant = DemanderMontant("déposer");
						toAccess.Depot(montant);
						break;
					}
					case ConsoleKey.R:
					{
						goodOperationChoice = true;
						exit = true;
						break;
					}
					default:
					{
						Console.WriteLine("Choix invalide. Recommencez.");
						break;
					}
				}
			}

			return exit;
		}

		public static Compte DemanderNumero(Banque b)
		{
			bool goodNumber = false;
			while (!goodNumber)
			{
				string numero = Console.ReadLine();
				if (b[numero] != null)
				{
					return b[numero];
				}

				Console.WriteLine("Mauvais numéro. Réessayez.");
			}

			return null;
		}

		private static int DemanderMontant(string action)
		{
			int montant;
			Console.WriteLine("Combien souhaitez-vous {0} ?", action);
			while (!Int32.TryParse(Console.ReadLine(), out montant))
			{
				Console.WriteLine("Ce n'est pas un montant valide. Réessayez.");
			}

			return montant;
		}
	}
}