﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque
{
	abstract class Compte
	{
		#region Properties

		public string Numero { get; set; }
		public virtual double Solde { get; private set; }
		public Personne Titulaire { get; set; }

		#endregion

		#region Methods

		public Compte(string numero, Personne titulaire)
		{
			Numero = numero;
			Titulaire = titulaire;
			Solde = 0;
		}

		public abstract bool Retrait(double montant);
		protected bool Retrait(double montant, double ligneDeCredit)
		{
			if (montant > 0 && Solde - montant >= -ligneDeCredit)
			{
				Solde -= montant;
				return true;
			}

			if (montant < 0)
			{
				throw new ArgumentException("Tentative de retrait d'un montant négatif.");
			}

			throw new Exception("Limite de crédit dépassée.");
		}

		public bool Depot(double montant)
		{
			if (montant > 0)
			{
				Solde += montant;
				return true;
			}

			throw new ArgumentException("Tentative de dépôt d'un montant négatif.");
		}

		public static double operator +(Compte c1, Compte c2)
		{
			double somme = 0;
			try
			{
				if (c1.Solde > 0)
				{
					somme += c1.Solde;
				}
			}
			catch (ArgumentNullException e)
			{
				return -1;
			}

			try
			{
				if (c2.Solde > 0)
				{
					somme += c2.Solde;
				}
			}
			catch (ArgumentNullException e)
			{
				return -1;
			}

			return somme;
		}

		public static double operator +(double d, Compte c)
		{
			try
			{
				if (c.Solde > 0)
				{
					return d + c.Solde;
				}
				return d;
			}
			catch (ArgumentNullException e)
			{
				return -1;
			}
		}

		protected abstract double CalculInteret();

		public void AppliquerInteret()
		{
			Solde += CalculInteret();
		}

		#endregion
	}
}
