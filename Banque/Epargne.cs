﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque
{
	class Epargne : Compte
	{
		#region Properties

		private DateTime? DateDernierRetrait { get; set; }

		#endregion

		#region Methods

		public Epargne(string numero, Personne titulaire) : base(numero, titulaire)
		{
			DateDernierRetrait = null;
		}

		public override bool Retrait(double montant)
		{
			bool effectue = base.Retrait(montant, 0);
			DateDernierRetrait = DateTime.Now;
			return effectue;
		}

		protected override double CalculInteret()
		{
			return Solde*0.45;
		}

		#endregion
	}
}