﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque
{
	class Personne
	{
		#region Properties
		public string Nom { get; set; }
		public string Prenom { get; set; }
		public DateTime DateNaiss { get; set; }
		#endregion
		#region Methods
		public Personne(string nom, string prenom)
		{
			Nom = nom;
			Prenom = prenom;
		}
		public Personne(string nom, string prenom, DateTime dateNaiss) : this(nom, prenom)
		{
			DateNaiss = dateNaiss;
		}
		#endregion
	}
}
