﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque
{
	class Pret: Compte
	{
		public Pret(string numero, Personne titulaire) : base(numero, titulaire)
		{
		}

		public override bool Retrait(double montant)
		{
			return false;
		}

		protected override double CalculInteret()
		{
			throw new NotImplementedException();
		}
	}
}
