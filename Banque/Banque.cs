﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque
{
	class Banque
	{
		#region Properties
		public Dictionary<string, Compte> Comptes { get; }
		public string Nom { get; set; }
		public Compte this[string numero]
		{
			get
			{
				try
				{
					return Comptes.TryGetValue(numero, out Compte c) ? c : null;
				}
				catch (ArgumentNullException e)
				{
					return null;
				}
			}
		}
		#endregion
		#region Methods
		public Banque(string nom, int size)
		{
			Nom = nom;
			Comptes = new Dictionary<string, Compte>();
		}

		public List<Courant> getComptesCourants()
		{
			List<Courant> ls = new List<Courant>();
			foreach (Compte compte in Comptes.Values)
			{
				if (compte is Courant)
				{
					ls.Add((Courant)compte);
				}
			}

			return ls;
		}

		public List<Epargne> getComptesEpargnes()
		{
			List<Epargne> ls = new List<Epargne>();
			foreach (Compte compte in Comptes.Values)
			{
				if (compte is Epargne)
				{
					ls.Add((Epargne)compte);
				}
			}

			return ls;
		}

		public bool Ajouter(Compte compte)
		{
			try
			{
				if (!Comptes.TryGetValue(compte.Numero, out Compte c))
				{
					Comptes.Add(compte.Numero, compte);
					return true;
				}
				return false;
			}
			catch (ArgumentNullException e)
			{
				return false;
			}
			catch (ArgumentException e)
			{
				return false;
			}
		}
		public bool Supprimer(string numero)
		{
			try
			{
				return Comptes.Remove(numero);
			}
			catch (ArgumentNullException e)
			{
				return false;
			}
		}
		public bool Supprimer(Compte compte)
		{
			return Supprimer(compte.Numero);
		}
		public double AvoirDesComptes(string nom, string prenom, DateTime dateNaiss)
		{
			double avoir = 0;
			foreach (Compte compte in Comptes.Values)
			{
				if (compte.Titulaire.Nom == nom && compte.Titulaire.Prenom == prenom && compte.Titulaire.DateNaiss == dateNaiss)
				{
					avoir += compte;
				}
			}
			return avoir;
		}
		public double AvoirDesComptes(Personne personne)
		{
			try
			{
				return AvoirDesComptes(personne.Nom, personne.Prenom, personne.DateNaiss);
			}
			catch (ArgumentNullException e)
			{
				return -1;
			}
		}

		public string GenererNumero()
		{
			return "1234";
		}
		#endregion
	}
}
