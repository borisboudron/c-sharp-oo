﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRace
{
	class Race
	{
		public Race(int lapsCount, int lapLength, Random random)
		{
			LapsCount = lapsCount;
			LapLength = lapLength;
			Racers = new List<Car>();
		}

		public int LapsCount { get; set; }
		public int LapLength { get; set; }
		private List<Car> Racers { get; }
		public Car this[int index]
		{
			get
			{
				if (index >= 0 && index < Racers.Count)
				{
					return Racers[index];
				}
				return null;
			}
			set
			{
				Racers[index] = value;
			}
		}
		public void AddCar(Car car)
		{
			if (!Racers.Contains(car))
			{
				Racers.Add(car);
			}
		}
		public void StartRace(Random random)
		{
			foreach (Car car in Racers)
			{
				car.LapTimes = new List<double>();
				for (int l = 0; l < LapsCount; l++)
				{
					car.RunLap(l, LapLength, random);
				}
			}
		}
		public void Results()
		{
			for (int l = 1; l <= LapsCount; l++)
			{
				Racers.Sort((r1, r2) =>
				{
					if (r1.LapTimes.Take(l).Sum() == r2.LapTimes.Take(l).Sum()) return 0;
					return (r1.LapTimes.Take(l).Sum() < r2.LapTimes.Take(l).Sum() ? -1 : 1);
				});
				Console.WriteLine("Tour {0}/{1} :", l, LapsCount);
				int position = 1;
				foreach (Car car in Racers)
				{
					Console.WriteLine("{0}. {1} : {2} (Tour : {3})", position, car.Pilot.NameWithSpaces(15), new LapTime(car.LapTimes.Take(l).Sum()).ToString(), new LapTime(car.LapTimes[l - 1]).ToString());
					position++;
				}
			}
			Console.WriteLine("Résultats :");
			int finalPosition = 1;
			foreach (Car car in Racers)
			{
				Console.WriteLine("{0}. {1} : {2}", finalPosition, car.Pilot.NameWithSpaces(15), new LapTime(car.LapTimes.Sum()).ToString());
				finalPosition++;
			}
			Console.WriteLine("Record du tour :");
			Racers.Sort((r1, r2) =>
			{
				if (r1.LapTimes.Min() == r2.LapTimes.Min()) return 0;
				return (r1.LapTimes.Min() < r2.LapTimes.Min() ? -1 : 1);
			});
			Console.WriteLine("{0} : {1} (Tour {2})", Racers[0].Pilot.NameWithSpaces(18), new LapTime(Racers[0].LapTimes.Min()).ToString(), Racers[0].LapTimes.IndexOf(Racers[0].LapTimes.Min()) + 1);
		}
		public void Podium()
		{
			Racers.Sort((r1, r2) =>
			{
				if (r1.LapTimes.Sum() == r2.LapTimes.Sum()) return 0;
				return (r1.LapTimes.Sum() < r2.LapTimes.Sum() ? -1 : 1);
			});
			Console.WriteLine("Podium :");
			Console.WriteLine("                    {0}                   ", Racers[0].Pilot.NameBetweenSpaces(20));
			Console.WriteLine("                    ____________________                    ");
			Console.WriteLine("                    |------ ____ ------|                    ");
			Console.WriteLine("{0}|------/_   |------|                    ", Racers[1].Pilot.NameBetweenSpaces(20));
			Console.WriteLine("____________________|------ |   |------|                    ");
			Console.WriteLine("|----________  ------------ |   |------|                    ");
			Console.WriteLine("|----\\_____  \\ ------------ |___|------|{0}", Racers[2].Pilot.NameBetweenSpaces(20));
			Console.WriteLine("|---- /  ____/ ------------------------|____________________");
			Console.WriteLine("|----/       \\  -----------------------------________  ----|");
			Console.WriteLine("|----\\_______ \\ -----------------------------\\_____  \\ ----|");
			Console.WriteLine("|----        \\/ -----------------------------  _(__  < ----|");
			Console.WriteLine("|-------------------------------------------- /       \\----|");
			Console.WriteLine("|--------------------------------------------/______  /----|");
			Console.WriteLine("|--------------------------------------------       \\/ ----|");
		}
	}
}
