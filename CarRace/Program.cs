﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRace
{
	class Program
	{
		static void Main(string[] args)
		{
			Random random = new Random();
			Race race = new Race(5, 7, random);
			foreach (Pilots p in Enum.GetValues(typeof(Pilots)))
			{
				Car car = new Car(new Pilot(p, random), random);
				race.AddCar(car);
			}
			race.StartRace(random);
			race.Results();
			race.Podium();

			Console.ReadLine();
		}
		public static double GetRandomNumber(Random random, double minimum, double maximum)
		{
			return random.NextDouble() * (maximum - minimum) + minimum;
		}
	}
}
