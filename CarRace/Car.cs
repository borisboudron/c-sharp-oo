﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRace
{
	enum Constructors
	{
		Ferrari,
		Mercedes,
		RedBull,
		McLaren
	}
	class Car
	{
		public Car(Pilot pilot, Random random)
		{
			Pilot = pilot;
			Constructor = (Constructors)random.Next(1, Enum.GetNames(typeof(Constructors)).Length);
			LapTimes = null;
		}
		public Constructors Constructor { get; set; }
		public Pilot Pilot { get; set; }
		public List<double> LapTimes { get; set; }
		public override string ToString()
		{
			return "Voiture : " + Constructor.ToString() + " : pilote : " + Pilot.ToString();
		}
		public void RunLap(int lap, double lapLength, Random random)
		{
			double efficiency = Program.GetRandomNumber(random, 85, 101) / 100;
			double lapTime = lapLength * 3600 / (Pilot.MaxSpeed * efficiency);
			LapTimes.Add(lapTime);
		}
	}
	struct LapTime
	{
		public int Hours { get; set; }
		public int Minutes { get; set; }
		public int Seconds { get; set; }
		public int Milliseconds { get; set; }
		public LapTime(double seconds)
		{
			Hours = (int)Math.Floor(seconds / 3600);
			Minutes = (int)Math.Floor((seconds - Hours * 3600) / 60);
			Seconds = (int)Math.Floor(seconds - Hours * 3600 - Minutes * 60);
			Milliseconds = (int)Math.Floor((seconds - Hours * 3600 - Minutes * 60 - Seconds) * 1000);
		}
		public string ZeroFill(double number, int charNumber)
		{
			string zeroFilled = "";
			int i = charNumber;
			bool found = false;
			while (!found && i > 0)
			{
				if (number < Math.Pow(10, i - 1))
				{
					zeroFilled += "0";
				}
				else
				{
					found = true;
					zeroFilled += number.ToString();
				}
				i--;
			}
			return zeroFilled;
		}
		public override string ToString()
		{
			string hours = ZeroFill(Hours, 2);
			string minutes = ZeroFill(Minutes, 2);
			string seconds = ZeroFill(Seconds, 2);
			string milliseconds = ZeroFill(Milliseconds, 3);
			return hours + ':' + minutes + ':' + seconds + '.' + milliseconds;
		}
	}
}