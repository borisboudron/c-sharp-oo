﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRace
{
	enum Pilots
	{
		Hamilton,
		Vettel,
		Raikkonen,
		Alonso,
		Verstappen,
		Ricciardo
	}
	class Pilot
	{
		public Pilot(Pilots name, Random random)
		{
			Name = name;
			MaxSpeed = GenerateSpeed(random);
		}
		public Pilots Name { get; set; }
		public double MaxSpeed { get; set; }
		public override string ToString()
		{
			return Name.ToString() + " : vitesse maximum : " + MaxSpeed.ToString() + "km/h";
		}

		public static double GenerateSpeed(Random r)
		{
			return r.Next(200, 221);
		}
		public string NameWithSpaces(int charNumber)
		{
			string withSpaces = Name.ToString();
			for (int i = Name.ToString().Length; i < charNumber; i++)
			{
				withSpaces += " ";
			}
			return withSpaces;
		}
		public string NameBetweenSpaces(int charNumber)
		{
			string betweenSpaces = "";
			double spaces = charNumber - Name.ToString().Length;
			for (int i = 0; i < Math.Floor(spaces/2); i++)
			{
				betweenSpaces += " ";
			}
			betweenSpaces += Name.ToString();
			for (int i = 0; i < Math.Ceiling(spaces / 2); i++)
			{
				betweenSpaces += " ";
			}
			return betweenSpaces;
		}
	}
}
