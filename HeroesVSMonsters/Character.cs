﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesVSMonsters
{
	abstract class Character
	{
		private int _stamina;

		public virtual int Stamina
		{
			get { return _stamina; }
			private set { _stamina = value; }
		}
		private int _strength;

		public virtual int Strength
		{
			get { return _strength; }
			private set { _strength = value; }
		}
		
		public virtual int Health { get; private set; }
		public int Gold { get; set; }
		public int Leather { get; set; }

		public Character()
		{
			Stamina = GenerateStat();
			Strength = GenerateStat();
			Health = Stamina + GetBonus(Stamina);
		}

		public override string ToString()
		{
			return $"Personnage de type {GetStringType()}, d'endurance {Stamina}, de force {Strength} possédant {Health} PV, {Gold} pièces d'or et {Leather} unités de cuir.";
		}

		protected abstract string GetStringType();

		private int GetBonus(int stat)
		{
			int bonus;
			if (stat < 5)
				bonus = -1;
			else if (stat < 10)
				bonus = 0;
			else if (stat < 15)
				bonus = 1;
			else
				bonus = 2;

			return bonus;
		}

		private int GenerateStat()
		{
			List<int> results = new List<int> { Dice.Throw(6), Dice.Throw(6), Dice.Throw(6), Dice.Throw(6) };
			int min = results.ElementAt(0);
			for (int i = 1; i < results.Count; i++)
			{
				min = Math.Min(min, results.ElementAt(i));
			}
			results.Remove(min);
			return results.Sum();
		}

		public void Attack(Character defender)
		{
			int damage = Dice.Throw(4) + GetBonus(Strength);
			defender.LoseHealth(damage);
			if (defender.Health == 0)
			{
				//				StealLoot(defender);
			}
		}

		private void LoseHealth(int damage)
		{
			Health = Math.Max(0, Health - damage);
		}
	}
}
