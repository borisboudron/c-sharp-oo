﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesVSMonsters
{
	class Wolf : Monster
	{
		public Wolf() : base()
		{
			Gold = 0;
			Leather = Dice.Throw(4);
		}

		protected override string GetStringType()
		{
			return "Loup";
		}
	}
}
