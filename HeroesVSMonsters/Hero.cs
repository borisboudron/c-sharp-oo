﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesVSMonsters
{
	abstract class Hero : Character
	{
		public Hero() : base()
		{
			Gold = 0;
			Leather = 0;
		}
		public void FightEnemy(Monster enemy)
		{
			int first = Dice.Throw(2);
			Character attacker = first == 1 ? (Character)this : (Character)enemy;
			Character defender = first == 2 ? (Character)this : (Character)enemy;
			while (Health > 0 && enemy.Health > 0)
			{
				attacker.Attack(defender);
				Character temp = attacker;
				attacker = defender;
				defender = temp;
			}

			if (Health > 0)
				StealLoot(enemy);
		}

		private void StealLoot(Monster enemy)
		{
			Gold += enemy.Gold;
			Leather += enemy.Leather;
		}
	}
}
