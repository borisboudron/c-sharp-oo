﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesVSMonsters
{
	class Dwarf : Hero
	{
		private static int staminaBonus = 2;
		public override int Stamina
		{
			get { return base.Stamina + staminaBonus; }
		}

		protected override string GetStringType()
		{
			return "Nain";
		}
	}
}
