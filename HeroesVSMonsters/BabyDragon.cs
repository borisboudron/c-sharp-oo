﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesVSMonsters
{
	class BabyDragon : Monster
	{
		private static int staminaBonus = 1;
		public override int Stamina
		{
			get { return base.Stamina + staminaBonus; }
		}

		public BabyDragon() : base()
		{
			Gold = Dice.Throw(6);
			Leather = Dice.Throw(4);
		}

		protected override string GetStringType()
		{
			return "Dragonnet";
		}
	}
}
