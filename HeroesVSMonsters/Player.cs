﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesVSMonsters
{
	class Player
	{
		public List<Dice2> dices { get; set; }

		public Player()
		{
			dices = new List<Dice2>();
		}
	}
}
