﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesVSMonsters
{
	class Orc : Monster
	{
		private static int strengthBonus = 1;
		public override int Strength
		{
			get { return base.Strength + strengthBonus; }
		}
		public Orc() : base()
		{
			Gold = Dice.Throw(6);
			Leather = 0;
		}

		protected override string GetStringType()
		{
			return "Orque";
		}
	}
}
