﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesVSMonsters
{
	static class Dice
	{
		public static Random Random { get; } = new Random();
		
		public static int Throw(int facesNb)
		{
			return Random.Next(1, facesNb + 1);
		}
	}
}
