﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesVSMonsters
{
	class Human : Hero
	{
		private static int staminaBonus = 1;
		private static int strengthBonus = 1;
		public override int Stamina
		{
			get { return base.Stamina + staminaBonus; }
		}
		public override int Strength
		{
			get { return base.Strength + strengthBonus; }
		}

		protected override string GetStringType()
		{
			return "Humain";
		}
	}
}
