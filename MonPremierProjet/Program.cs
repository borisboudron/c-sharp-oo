﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonPremierProjet
{
	class Program
	{
		static void Main(string[] args)
		{
			// Déclaration et initialisation des variables
			List<int> liste = new List<int>();
			bool stop = false;

			// Tant que l'utilisateur ne quitte pas, lui demander un nombre, analyser ce qu'il a rentré et agir en conséquence
			while (!stop)
			{
				Console.WriteLine("Entrez un nombre ou q pour quitter");
				string input = Console.ReadLine();
				// Si l'entrée est convertible, on stocke le nombre
				if (Int32.TryParse(input, out int number))
				{
					liste.Add(number);
				}
				// Si l'entrée est Q ou q, on quitte
				else if (input == "q" || input == "Q")
				{
					stop = true;
				}
				// Sinon c'est une mauvaise entrée
				else
				{
					Console.WriteLine("Ceci n'est pas un nombre");
				}
			}

			// Ecrit chaque nombre de la liste et le copie dans une autre liste
			foreach (int number in liste)
			{
				Console.Write($"{number} > ");
			}
			Console.WriteLine();

			// Trie l'autre liste
			for (int i = 0; i < liste.Count - 1; i++)
			{
				int pos = i;
				for (int j = i + 1; j < liste.Count; j++)
				{
					if (liste[j] < liste[pos])
					{
						pos = j;
					}
				}
				if (pos != i)
				{
					liste[pos] += liste[i];
					liste[i] = liste[pos] - liste[i];
					liste[pos] -= liste[i];
				}
			}

			// Ecrit chaque nombre de la liste triée
			foreach (int number in liste)
			{
				Console.Write($"{number} > ");
			}
			Console.WriteLine();
			Console.ReadLine();
		}
	}
}
